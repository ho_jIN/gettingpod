# GettingPod

[![CI Status](http://img.shields.io/travis/hojin@hsociety.co.kr/GettingPod.svg?style=flat)](https://travis-ci.org/hojin@hsociety.co.kr/GettingPod)
[![Version](https://img.shields.io/cocoapods/v/GettingPod.svg?style=flat)](http://cocoapods.org/pods/GettingPod)
[![License](https://img.shields.io/cocoapods/l/GettingPod.svg?style=flat)](http://cocoapods.org/pods/GettingPod)
[![Platform](https://img.shields.io/cocoapods/p/GettingPod.svg?style=flat)](http://cocoapods.org/pods/GettingPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GettingPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GettingPod'
```

## Author

hojin@hsociety.co.kr, hojin@hsociety.co.kr

## License

GettingPod is available under the MIT license. See the LICENSE file for more info.
